#!/usr/bin/env bash

while true; do
    xsetroot -name "$(bar-cmus.sh) | $(bar-volume.sh) | $(bar-diskspace.sh) | $(bar-cpu.sh) | $(bar-memory.sh) | $(bar-bandwidth.sh) | $(bar-upt.sh) | $(bar-date.sh)"
    sleep 10s
done
