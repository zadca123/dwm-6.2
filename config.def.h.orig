/* See LICENSE file for copyright and license details. */
/* patch for that!@!@ */
#define TERMINAL "kitty"

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 5;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "iosevka:size=10:style=bold," };
static const char dmenufont[]       = "iosevka:size=10:style=bold";
static const char col_gray1[]       = "#282828"; //background color
static const char col_gray2[]       = "#444444"; //inactive window border color
static const char col_gray3[]       = "#ebdbb2"; //font color
static const char col_gray4[]       = "#eeeeee"; //current tag and current window font color
static const char col_cyan[]        = "#98971a"; //Top bar second color (def. blue) and active window border color
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",    NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "Rox",     NULL,     NULL,           0,         1,          0,           0,        -1 },
	{ "Firefox", NULL,     NULL,           0 << 8,    0,          0,          -1,        -1 },
	{ "St",      NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "kitty",   NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
 	{ "[@]",      spiral },
 	{ "[\\]",      dwindle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { TERMINAL, NULL };

#include <X11/XF86keysym.h>
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_Tab,    focusstack,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Tab,    focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_a,      view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },

    /* Programs launched with ALT + SHIFT + KEY */
	{ Mod1Mask|ShiftMask, XK_b,      spawn,          SHCMD("palemoon") },
	{ Mod1Mask|ShiftMask, XK_y,      spawn,          SHCMD("freetube") },
	{ Mod1Mask|ShiftMask, XK_t,      spawn,          SHCMD("~/Documents/TeamSpeak3-Client-linux_amd64/ts3client_runscript.sh") },
	{ Mod1Mask|ShiftMask, XK_e,      spawn,          SHCMD("emacsclient -a '' -c") },
    /* Programs launched with ALT + CTRL + KEY */
	{ Mod1Mask|ControlMask, XK_a,      spawn,          SHCMD(TERMINAL " -e pulsemixer") },
	{ Mod1Mask|ControlMask, XK_d,      spawn,          SHCMD(TERMINAL " -e stig") },
	{ Mod1Mask|ControlMask, XK_t,      spawn,          SHCMD(TERMINAL " -e tmux-session.sh") },
	{ Mod1Mask|ControlMask, XK_minus,  spawn,          SHCMD("cmus-notify.sh -k -1m") },
	{ Mod1Mask|ControlMask, XK_equal,  spawn,          SHCMD("cmus-notify.sh -k +1m") },
	{ Mod1Mask|ControlMask, XK_m,      spawn,          SHCMD("screenshot.sh fullscreen") },
	{ Mod1Mask|ControlMask, XK_s,      spawn,          SHCMD("screenshot.sh selection") },
	{ Mod1Mask|ControlMask, XK_c,      spawn,          SHCMD("screenshot.sh clipboard") },
	{ Mod1Mask|ControlMask, XK_f,      spawn,          SHCMD("screenshot.sh focused") },
	{ Mod1Mask|ControlMask, XK_u,      spawn,          SHCMD("screenshot.sh selection upload") },
	{ Mod1Mask|ControlMask, XK_1,      spawn,          SHCMD("screenshot.sh 16x9") },
	{ Mod1Mask|ControlMask, XK_4,      spawn,          SHCMD("screenshot.sh 4x3") },
    /* Programs launched with ALT + CTRL + KEY */
	{ Mod1Mask,             XK_p,      spawn,          SHCMD("rofi -show drun -show-icons") },
	{ Mod1Mask,             XK_Tab,    spawn,          SHCMD("rofi -show window -show-icons") },
	{ Mod1Mask,             XK_s,      spawn,          SHCMD("redshift -b 0.95 -O 6000K") },
	{ Mod1Mask,             XK_r,      spawn,          SHCMD("redshift -x") },
	{ Mod1Mask,             XK_n,      spawn,          SHCMD("nitrogen --set-auto --random") },
	{ Mod1Mask,             XK_minus,  spawn,          SHCMD("cmus-notify.sh --volume -1%") },
	{ Mod1Mask,             XK_equal,  spawn,          SHCMD("cmus-notify.sh --volume +1%") },
    /* Programs launched with SPECIAL KEYS (patch for that!@!@)*/
	{ 0, XF86XK_AudioMute,		    spawn,		SHCMD("pactl set-sink-mute @DEFAULT_SINK@ toggle") },
	{ 0, XF86XK_AudioRaiseVolume,	spawn,		SHCMD("dunst-volume-indicator.sh 2dB+") },
	{ 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("dunst-volume-indicator.sh 2dB-") },
	{ 0, XF86XK_Tools,		        spawn,		SHCMD("cmus-notify.sh -Q") },
	{ 0, XF86XK_AudioPrev,		    spawn,		SHCMD("cmus-notify.sh -r") },
	{ 0, XF86XK_AudioNext,		    spawn,		SHCMD("cmus-notify.sh -n") },
	{ 0, XF86XK_AudioPause,		    spawn,		SHCMD("cmus-notify.sh -u") },
	{ 0, XF86XK_AudioPlay,		    spawn,		SHCMD("cmus-notify.sh -u") },
	{ 0, XF86XK_AudioStop,		    spawn,		SHCMD("cmus-notify.sh -s") },
	{ 0, XF86XK_AudioRewind,	    spawn,		SHCMD("cmus-notity.sh -k -1m") },
	{ 0, XF86XK_AudioForward,	    spawn,		SHCMD("cmus-notity.sh -k +1m") },
	{ 0, XF86XK_AudioMedia,		    spawn,		SHCMD(TERMINAL " -e tmux-session.sh") },
	{ 0, XF86XK_AudioMicMute,	    spawn,		SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0, XF86XK_PowerOff,		    spawn,		SHCMD("systemctl poweroff") },
	{ 0, XF86XK_Calculator,		    spawn,		SHCMD(TERMINAL " -e qalc") },
	{ 0, XF86XK_WWW,		        spawn,		SHCMD("palemoon") },
	{ 0, XF86XK_DOS,		        spawn,		SHCMD(TERMINAL) },
	{ 0, XF86XK_HomePage,		    spawn,		SHCMD(TERMINAL " -e vifm") },
	{ 0, XF86XK_Mail,		        spawn,		SHCMD(TERMINAL " -e aerc") },
	{ 0, XF86XK_Explorer,		    spawn,		SHCMD(TERMINAL " -e btop") },
	{ 0, XF86XK_MonBrightnessUp,	spawn,		SHCMD("dunst-backlight-indicator.sh -inc 1") },
	{ 0, XF86XK_MonBrightnessDown,	spawn,		SHCMD("dunst-backlight-indicator.sh -dec 2") },

	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

